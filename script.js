let imgList = document.querySelectorAll('.img');
let imgPreview = document.querySelector('.img');
let btnNext = document.querySelector('.btn-next');
let btnPrev = document.querySelector('.btn-prev');
let dotsBtn = document.querySelectorAll('.dots');
const containerImg = document.querySelector('.container');
let index = 1;


// btn next slider
btnNext.addEventListener('click', function () {
    index++;
    if (index > Array.from(imgList).length) {
        index = 1;
    };

    slideShow(index);
    let dotActive = document.querySelector('.active');
    dotActive.classList.remove('active');
    dotsBtn[index - 1].classList.add('active');
});


// btn-prev-slider
btnPrev.addEventListener('click', function () {
    index--;
    if (index <= 0) {
        index = Array.from(imgList).length;
    }
    slideShow(index);

    let dotActive = document.querySelector('.active');
    dotActive.classList.remove('active');
    dotsBtn[index - 1].classList.add('active');
});



function slideShow(index) {
    imgPreview.src = `./img/anh${index}.jpg`;
}
dotsBtn[0].classList.add('active');


// active dot
function addActive() {
    Array.from(dotsBtn).forEach(function (dot, index) {
        dot.onclick = (e) => {
            const index = e.target.getAttribute("data");
            slideShow(index);

            let dotActive = document.querySelector('.active');
            dotActive.classList.remove('active');
            e.target.classList.add('active');
        };
    });
};
addActive();


//update them drag
let isDragging = false;
let startPosition = 0;
let deltaX = 0;

// khi click giu tren anh
imgList.forEach((img) => {
    img.addEventListener('mousedown', (e) => {
        isDragging = true;

        startPosition = e.clientX - deltaX;
        e.preventDefault();

    });
});

// khi di chuyen cursor quan phan tu
document.addEventListener('mousemove', (e) => {
    if (isDragging === false) return;
    const newPosition = e.clientX - startPosition;
    deltaX = newPosition;
});

// khi tha ra tren 1 phan tu
document.addEventListener('mouseup', () => {
    isDragging = false;
    // Kiểm tra xem nếu di chuyển đủ xa thì chuyển đến slider mới
    if (deltaX > 50) {
        showImage(index - 1);
    } else if (deltaX < -50) {
        showImage(index + 1);
    }
    deltaX = 0;
});

function showImage(newIndex) {
    if (newIndex < 1) {
        newIndex = imgList.length;
    } else if (newIndex > imgList.length) {
        newIndex = 1;
    }

    index = newIndex;

    // Hiển thị ảnh mới
    slideShow(index);

    //  active dot
    let dotActive = document.querySelector('.active');
    dotActive.classList.remove('active');
    dotsBtn[index - 1].classList.add('active');
};


// setInterval(function () {
//     btnNext.click();
// }, 2000);